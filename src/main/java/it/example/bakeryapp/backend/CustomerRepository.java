package it.example.bakeryapp.backend;

import org.springframework.data.jpa.repository.JpaRepository;

import it.example.bakeryapp.backend.data.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
