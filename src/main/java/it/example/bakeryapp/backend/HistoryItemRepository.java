package it.example.bakeryapp.backend;

import org.springframework.data.jpa.repository.JpaRepository;

import it.example.bakeryapp.backend.data.entity.HistoryItem;

public interface HistoryItemRepository extends JpaRepository<HistoryItem, Long> {
}
